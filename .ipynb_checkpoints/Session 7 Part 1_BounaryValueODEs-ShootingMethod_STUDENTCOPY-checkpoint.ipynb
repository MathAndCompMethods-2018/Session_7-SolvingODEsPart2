{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Solving Ordinary Differential Equations\n",
    "# Session 7 Part 1:  Boundary Value Problems"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "from matplotlib import pyplot as plt\n",
    "\n",
    "import scipy.integrate as integrate\n",
    "import scipy.optimize as optimize\n",
    "\n",
    "from IPython.html.widgets import *\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "--- \n",
    "\n",
    "# Refresher from last week: Initial Value Problems\n",
    "\n",
    "Q. What sort of problems can `odeint` solve directly?\n",
    "\n",
    "The examples we looked at last time included:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### A 1st order ODE\n",
    "$$\\frac{dy}{dt}=y(1-t)$$ \n",
    "\n",
    "with the boundary condition $y(t=0)=0.1$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### A set of coupled ODEs\n",
    "\n",
    "$$\\frac{dy}{dt}=-x$$\n",
    "\n",
    "$$\\frac{dx}{dt}=y$$      \n",
    "      \n",
    "with initial conditions $x(t=0)=1 \\mbox{ and } y(t=0)=0$.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### A second (or higher) order ODE\n",
    "\n",
    "  $$\\frac{d^2y}{dt^2} +2\\frac{dy}{dt} +2y = \\cos(2t)$$ \n",
    "\n",
    "with boundary conditions $y(t=0)=0$ and $y'(t=0)=0$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "**NOTE:** \n",
    "All of these problems ONLY specified boundary conditions at t=0 !!! <p> This makes them \"Initial Value Problems\"</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "---\n",
    "\n",
    "# What are Boundary Value Problems and why can't `odeint` solve them directly?\n",
    "\n",
    "An example of a boundary value problem is:\n",
    "\n",
    "$$ \\frac{d^2y}{dx^2}=x\\sin x \\quad y(0)=y(\\pi)=0 $$\n",
    "\n",
    "because we have specified BCs at 2 different times and/or places!\n",
    "\n",
    "`odeint` requires that we specify ALL boundary conditions at the SAME time and/or place"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Question:\n",
    "- What is the minimum order ODE required for a problem to be a boundary problem?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "---\n",
    "<img src=\"http://slideplayer.com/slide/1474150/4/images/3/Daniel+Baur+/+Numerical+Methods+for+Chemical+Engineers+/+BVP+and+PDE.jpg\" align=\"RIGHT\" width=\"50%\">\n",
    "\n",
    "\n",
    "# 2. Solution 1: The Shooting Method \n",
    "## (Can be used for non-linear problems)\n",
    "\n",
    "These methods add another step into what you have seen using `odeint` in order to match the solution at 2 different places.\n",
    "\n",
    "- Sometimes we need to solve problems where boundary conditions\n",
    "    are not specified at the _same_ coordinate.\n",
    "- One approach used to solve these problems is to set the BC(s) at one boundary, then make a first guess which extrapolates to the other BC(s) and then try and improve on the guess until the BC(s) are all matched.\n",
    "- This is often more an art  than a science.\n",
    "\n",
    "This approach is called the _shooting_ method and we will consider relatively simple cases. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 2.1 A motivating example for using the shooting method\n",
    "\n",
    "We want to solve the problem $$\\frac{d^2y}{dx^2}+y=0$$ \n",
    "\n",
    "but now with the boundary conditions\n",
    "\n",
    "$$y(x=0)=1/2$$ \n",
    "$$y(x=\\pi/3)=1/2$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "We first create 2 coupled equations from $$\\frac{d^2y}{dx^2}+y=0$$\n",
    "\n",
    "Let $$z = \\dot{y}$$\n",
    "\n",
    "Substituting this into the differential equation: \n",
    "\n",
    "$$\\dot{z}=-y$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "So, if:\n",
    "\n",
    "$$\\mathbf{U} = \\begin{pmatrix} y \\\\ z \\end{pmatrix}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Therefore, the derivative is:\n",
    "  $$\\mathbf{\\dot{U}} =  \\begin{pmatrix}\\dot{y} \\\\ \\dot{z}  \\end{pmatrix} =  \\begin{pmatrix}z \\\\ -y \\end{pmatrix} =  \\begin{pmatrix} U[1] \\\\ -U[0] \\end{pmatrix}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "The boundary conditions are: $$y\\, (x=0)=1/2$$ and $$y\\, (x=\\pi/3))=1/2$$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "- We can directly set the BC $y(x=0)=1/2 $ \n",
    "\n",
    "- We can also specify a BC on the deivative of $y(x=0)$ really easily. (This is what we did before for the intial value problems.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "- The problem is that the boundary condition we need to satisfy is $y(x=\\pi/3)=1/2$.\n",
    "\n",
    "One way to do this is to try different values for the BC on $z(x=0)=\\dot{y}(x=0)$ until we get the right answer at $y(x=\\pi/3)$. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Let's demonstrate this with widgets before we solve it formally:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "## Define the function to return the derivatives\n",
    "def derivatives(U, x):\n",
    "    y = U[0]\n",
    "    z = U[1]\n",
    "    dy_dx = z\n",
    "    dz_dx = -y\n",
    "    return [dy_dx, dz_dx]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "## Define a function that plots the trajectory as a function of varying the initial gradient\n",
    "def pltShoot(dy_dx):\n",
    "\n",
    "    x=np.linspace( 0 , np.pi/3 ) # times for plotting\n",
    "    ic = np.array( [ 0.5, dy_dx ] ) # initial conditions\n",
    "\n",
    "    U = integrate.odeint(derivatives, ic ,x)\n",
    "\n",
    "    plt.plot(x, U)\n",
    "    plt.axhline(y=0.5, linestyle='dashed')\n",
    "    plt.axvline(x=np.pi/3, linestyle='dashed')\n",
    "    plt.legend(('y', \"dy_dx\"), loc='lower left')\n",
    "    plt.xlabel('x')\n",
    "    plt.ylabel('y, dy/dt')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "We need the boundary conditions $$y\\, (x=0)=1/2$$ and $$y\\, (x=\\pi/3))=1/2$$ "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "interact(pltShoot, dy_dx=FloatSlider(min=0.1,max=2) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "This is obviously a bit tedious so we want to automate it - that is what we do next!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# ASIDE: Click on the link to the [Finding the roots of a function notebook](Session 7 Part 2_FindingRootsOfAFuction_STUDENTCOPY)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "---\n",
    "\n",
    "## 2.2 The Shooting Method: Iteratively  applying `odeint` to match a distance BC\n",
    "\n",
    "So can now put `integrate.odeint` and `optimize.fsolve`,  `optimize.minimize` or `optimize.basinhopping` together.\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "<b>REMINDER of the problem (COPIED FROM SECTION 3.1)</b>\n",
    "\n",
    "We want to solve the problem $$\\frac{d^2y}{dx^2}+y=0$$ \n",
    "\n",
    "but now with the boundary conditions\n",
    "\n",
    "$$y(x=0)=1/2$$ \n",
    "$$y(x=\\pi/3)=1/2$$\n",
    "\n",
    "We first create 2 coupled equations from this second order differential,\n",
    "\n",
    "Let $z = \\dot{y}$\n",
    "\n",
    "Therefore, $$\\dot{z}=-y$$\n",
    "\n",
    "So, if:\n",
    "\n",
    "$$\\mathbf{U} = \\begin{pmatrix} y \\\\ z \\end{pmatrix}$$\n",
    "\n",
    "Therefore, the derivative is:\n",
    "  $$\\mathbf{\\dot{U}} =  \\begin{pmatrix}\\dot{y} \\\\ \\dot{z}  \\end{pmatrix} =  \\begin{pmatrix}z \\\\ -y \\end{pmatrix} =  \\begin{pmatrix} U[1] \\\\ -U[0] \\end{pmatrix}$$\n",
    "\n",
    "\n",
    "with the boundary conditions $$y\\, (x=0)=1/2$$ and $$y\\, (x=\\pi/3))=1/2$$ \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Now we need to guess many values of the BC on $z(x=0)=\\dot{y}(x=0)$ and find the one which matches the BC we actually know which is $y(x=\\pi/3)=1/2$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "We let the optimisation algorithms do the work for finding the closest value!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "The trick is to rearrange the problem to find roots - which we now know how to do. \n",
    "- Instead of searching for where the result of the integration equals 0.5, \n",
    "    - we rearrange it so that we look for where the integration minus 0.5 equals zero!    \n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "So we can use `fsolve` assuming our initial guess is good enough.\n",
    "\n",
    "Alternatively, we could use `minimize`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Let's look at some code that does this below!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "## Function that returns the coupled derivatives (needed for odeint)\n",
    "def derivatives(U, x):\n",
    "    y = U[0]\n",
    "    z = U[1]\n",
    "    dy_dx = z\n",
    "    dz_dx = -y\n",
    "    return [dy_dx, dz_dx]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 2.2a Implementing the shooting method using `optimize.fmin()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Define the function that integrates the function and returns\n",
    "## the difference between the trial solution and the boundary condtion\n",
    "## of z_1(np.pi/3) = 0.5. \n",
    "\n",
    "def integrate_delta( init_vel ):\n",
    "    \"\"\"function to be used by fsolve which integrates \n",
    "    F from 0 to pi/3 using the initial condtion\n",
    "    0.5, init_vel and subtracts 0.5\"\"\"\n",
    "    \n",
    "    ic = np.array([ 0.5 , init_vel ])  # The initial conditions\n",
    "\n",
    "    x  = np.array([0, np.pi/3])     # The range x to integrate over\n",
    "    \n",
    "    U  = integrate.odeint(derivatives, ic ,x) # Use odeint to integrate\n",
    "    \n",
    "    diff = U[-1,0] - 0.5            # How close is y(pi/3)=U[0](pi/3) to 0.5?\n",
    "    \n",
    "    return diff                     # This is what we want to find the root of"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# first guess for lambda (the dy_dx)\n",
    "dy_dx_initial_Guess = 0.1\n",
    "\n",
    "# Method 1: Use fsolve to find the ZERO value\n",
    "dy_dx_initial_Optimal = optimize.fsolve( integrate_delta, dy_dx_initial_Guess)\n",
    "\n",
    "print(\"Optimal initial gradient = \", str(dy_dx_initial_Optimal) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Having found the value of lambda, now integrate and plot results\n",
    "x=np.linspace(0,np.pi/3) # times for plotting\n",
    "\n",
    "# Compute the final z values using the optimised lambda we found above\n",
    "ic = np.array([ 0.5, dy_dx_initial_Optimal] ) # init cond\n",
    "\n",
    "U_best = integrate.odeint(derivatives, ic, x)\n",
    "\n",
    "# Plot the results\n",
    "plt.plot(x,U_best[:,0],'k+-')\n",
    "plt.plot(x,U_best[:,1],'b+-')\n",
    "plt.axhline(y=0.5, linestyle='dashed')\n",
    "plt.xlabel('x')\n",
    "plt.ylabel('y, dy_dx')\n",
    "plt.title(\"Optimal Solution using roots\")\n",
    "plt.legend(('y=U_best[:,0]', 'z=U_best[:,1]'), loc='center right')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 2.2b Implementing the shooting method using `optimize.minimize()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def integrate_sqr(init_vel):\n",
    "   \"\"\" function to be used by fmin which squares result \n",
    "   of function F (\"module\" variable) minus 0.5 at pi/3 \n",
    "   when integrated from 0 with initial condtions \n",
    "   of 0.5, init_vel \n",
    "   \"\"\"\n",
    "   targetValue = 0.5\n",
    "    \n",
    "   ic = np.array([0.5, init_vel])  # The initial conditions\n",
    "   x  = np.array([0, np.pi/3])     # The time range to integrate over\n",
    "   U  = integrate.odeint(derivatives, ic ,x) # Use odeint to integrate\n",
    "    \n",
    "   diff = ( U[-1,0] - targetValue )**2       # Return the square of the difference\n",
    "   return diff                               # This is what we want to minimise"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "dy_dx_initial_Guess = 0.1; # first guess for lambda (the initial velocity)\n",
    "\n",
    "# Method 2: Use fmin to find the MINIMUM value\n",
    "dy_dx_initial_Optimal = optimize.minimize(integrate_sqr, dy_dx_initial_Guess) \n",
    "\n",
    "print( \"Optimal initial dy_dx = \", str(dy_dx_initial_Optimal) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dy_dx_initial_Optimal['x'][0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Having found the value of lambda, now integrate and plot results\n",
    "x=np.linspace(0,np.pi/3) # times for plotting\n",
    "\n",
    "# Compute the final U values using the optimised lambda we found above\n",
    "ic = np.array( [0.5, dy_dx_initial_Optimal['x'] ]) # init cond\n",
    "U_final = integrate.odeint(derivatives, ic, x)\n",
    "\n",
    "# Plot the results\n",
    "plt.plot(x,U_final[:,0],'k+-')\n",
    "plt.plot(x,U_final[:,1],'b+-')\n",
    "plt.axhline(y=0.5, linestyle='dashed')\n",
    "plt.xlabel('x')\n",
    "plt.ylabel('y,  dy_dx')\n",
    "plt.title(\"Optimal Solution using minimise\")\n",
    "plt.legend(('y=U_best[:,0]', 'z=U_best[:,1]'), loc='center right')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3 EXERCISES:\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 3.1Use minimisation version of the shooting method to solve $y''+4y=0$ where $y(0)=1$ and $y'(\\pi)=2$\n",
    "\n",
    "Solve $y''+4y=0$ using the boundary conditions\n",
    "$y(0)=1$ and $y'(\\pi)=2$. \n",
    "\n",
    "Remember that you need to start by converting this second order ODE to a set of coupled 1st order ODEs.\n",
    "\n",
    "You can solve this problem analytically. \n",
    "\n",
    "Do so and compare your analytical solution with your Python one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Modify the code below:\n",
    "\n",
    "def derivatives(U,t):\n",
    "    y = ...\n",
    "    z = ...\n",
    "    dy_dt = ...\n",
    "    dz_dt = ...\n",
    "    return [..., ...]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t  = np.linspace(... , ...)      ## times -- only want start and end values\n",
    "\n",
    "def integrate_sqr( ... ):\n",
    "   \"\"\" function to be used by minimize which squares result \n",
    "   of function F (\"module\" variable) minus 0.5 at pi/3 \n",
    "   when integrated from 0 with initial condtions \n",
    "   of 0.5, init_vel \"\"\"\n",
    "   ic = np.array([ ... , ... ])    ## initial conditions\n",
    "   U  = integrate.odeint( ... , ... , ...) # do the integration\n",
    "   return (U[-1,1] - ... ) **...         # want distance^2 at end from 0.5\n",
    "\n",
    "lambda_0 = 2.; # first guess for lambda (the initial velocity)\n",
    "\n",
    "# Use minimize to find the value of lambda that match the distance BC\n",
    "lambda_fmin = optimize.minimize( ... , ... ) \n",
    "\n",
    "# Print the solution\n",
    "print( \"Lambda (fmin) = \", str(lambda_fmin['x']) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Now do the integration with the best fitting value so we can plot the result\n",
    "ic_best = np.array([ ... , ... )\n",
    "U = integrate.odeint( ... , ... , ...)\n",
    "\n",
    "plt.plot(t, U[:,0], t, U[:,1] )\n",
    "plt.xlabel('Time')\n",
    "plt.ylabel('z')\n",
    "plt.title(\"Final Value\")\n",
    "plt.legend(('y = U [:,0]', \"dy_dt = U [:,1]\"), loc='lower right')\n",
    "\n",
    "plt.axhline(y=2, color='green', linestyle='--')\n",
    "plt.axvline(x=np.pi, color='green', linestyle='--')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 3.2 The same with `fsolve`\n",
    "\n",
    "Now modify the solution above to work with `fsolve` instead of minimising the funciton."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python [default]",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  },
  "toc": {
   "colors": {
    "hover_highlight": "#DAA520",
    "running_highlight": "#FF0000",
    "selected_highlight": "#FFD700"
   },
   "nav_menu": {
    "height": "512px",
    "width": "252px"
   },
   "navigate_menu": true,
   "number_sections": false,
   "sideBar": true,
   "threshold": 4,
   "toc_cell": true,
   "toc_section_display": "block",
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
